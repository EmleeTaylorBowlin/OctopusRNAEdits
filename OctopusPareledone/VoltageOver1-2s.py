#!/usr/bin/env python
# coding: utf-8

# In[8]:


from brian2 import *
from matplotlib import *
start_scope()

tau = 60*ms
eqs = '''
dv/dt = (sin(2*pi*100*Hz*t)-v)/tau : 1
'''

# Change to Euler method because exact integrator doesn't work here
G = NeuronGroup(1, eqs, method='euler')
M = StateMonitor(G, 'v', record=0)

G.v = 590 # initial value

run(500*ms)

plot(M.t/ms, M.v[0])
xlabel('Time (ms)')
ylabel('Voltage');


# In[ ]:




