#!/usr/bin/env python
# coding: utf-8

# In[5]:

#!/usr/bin/env python
# coding: utf-8

# In[3]:


from brian2 import *
from matplotlib import *
start_scope()
# Parameters
area = 20000*umetre**2
Cm = 1*ufarad*mm**-2 * area
gl = 5e-5*siemens*mm**-2 * area
El = -50*mV
EK = -175*mV
ENa = -125*mV
g_na = 100*msiemens*mm**-2 * area
g_kd = 30*msiemens*mm**-2 * area
VT = -63*mV

durations_I_want_ms = [1*1000, 2*1000, 6*1000, 1*60*60*1000]

for duration_in_ms in durations_I_want_ms:
    duration_in_run_call = duration_in_ms / 50

    # The model
    eqs_HH = '''
dv/dt = (gl*(El-v) - g_na*(m*m*m)*h*(v-ENa) - g_kd*(n*n*n*n)*(v-EK) + I)/Cm : volt
dm/dt = 0.32*(mV**-1)*(13.*mV-v+VT)/
    (exp((13.*mV-v+VT)/(4.*mV))-1.)/ms*(1-m)-0.28*(mV**-1)*(v-VT-40.*mV)/
    (exp((v-VT-40.*mV)/(5.*mV))-1.)/ms*m : 1
dn/dt = 0.032*(mV**-1)*(15.*mV-v+VT)/
    (exp((15.*mV-v+VT)/(5.*mV))-1.)/ms*(1.-n)-.5*exp((10.*mV-v+VT)/(40.*mV))/ms*n : 1
dh/dt = 0.128*exp((17.*mV-v+VT)/(18.*mV))/ms*(1.-h)-4./(1+exp((40.*mV-v+VT)/(5.*mV)))/ms*h : 1
I : amp
'''
    group = NeuronGroup(1, eqs_HH,
                        threshold='v > -40*mV',
                        refractory='v > -40*mV',
                        method='exponential_euler')
    group.v = El
    statemon = StateMonitor(group, 'v', record=True)
    spikemon = SpikeMonitor(group, variables='v')
    for l in range(5):
        group.I = rand()*50*nA
        run(duration_in_run_call*ms)
        axvline(l*10, ls='--', c='k')
    axhline(El/mV, ls='-', c='lightgray', lw=3)
    plot(statemon.t/ms, statemon.v[0]/mV, '-b')
    plot(spikemon.t/ms, spikemon.v/mV, 'ob')
    xlabel('Time (ms)')
    ylabel('v (mV)')

    for ftype in ('pdf', 'png'):
        fname = f'spikes_{duration_in_ms/1000}sec.{ftype}'
        savefig(fname)
        print(f'# saved plot to file {fname}')

print('# done with all durations')
    
#show()


