# OctopusRNAEdits

This model shows the neuron changes in Octopus Vulagis and Octopus Pareledone SP, as they make RNA edits and adapt to temperature. 

You will need to install both, matplotlib and the python libary Brian

You can install brian on Linux/Ubuntu with sudo apt install python3-brian
You can install brian on Fedora with sudo dnf install python-brian2
You can install brian with a PyPi package with python -m pip install brian2 Then python -m ensurepip

matplotlib can be installed with python -m pip install -U pip
Then python -m pip install -U matplotlib

The last thing you will need is Jupyter Notebook : https://docs.jupyter.org/en/latest/install/notebook-classic.html
It can be installed with pip3 install jupyter *Make sure you have the newest version of pip*
 
How to run Jupyter:https://docs.jupyter.org/en/latest/running.html
in the command line type: jupyter notebook 
this will allow Jupyter to open

In Jupyter click new and open a new notebook
Paste any code you wish to run and then run with Jupyter
It should appear at the bottom of the notebook

This code should demonstrate the opening and closing rate of octopus nuerons as a mV over time graph

